/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.ArrayList;
import java.util.List;
import sk.uniq.protobuf.parser.ProtoErrorListener;
import sk.uniq.protobuf.parser.ProtoParserError;

/**
 *
 * @author jherkel
 */
public class ProtobufSyntaxErrorListener implements ProtoErrorListener {

    private final List<ProtoParserError> errors = new ArrayList<>();

    public List<ProtoParserError> getErrors() {
        return errors;
    }

    @Override
    public void protobufError(ProtoParserError error) {
        errors.add(error);
    }

}
