/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.Collections;
import java.util.Set;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoMessage;
import sk.uniq.protobuf.schema.ProtoRpc;
import sk.uniq.protobuf.schema.ProtoService;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;

/**
 *
 * @author jherkel
 */
public class ProtoElementImpl implements ProtoElement {

    private FileObject fileObject;
    private final ProtoSyntaxElement protoSyntaxElement;
    private final String name;
    private final OffsetRange offsetRange;

    public ProtoElementImpl(ProtoSyntaxElement protoSyntaxElement, FileObject fileObject, String name) {
        this.protoSyntaxElement = protoSyntaxElement;
        this.fileObject = fileObject;
        this.name = name;
        this.offsetRange = new OffsetRange(protoSyntaxElement.getPosition().getStartPos(), protoSyntaxElement.getPosition().getEndPos());
    }

    @Override
    public FileObject getFileObject() {
        return fileObject;
    }

    protected void setFileObject(FileObject fileObject) {
        this.fileObject = fileObject;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIn() {
        return null;
    }

    @Override
    public String getMimeType() {
        return ProtobufContant.PROTO_MIME_TYPE;
    }

    @Override
    public ElementKind getKind() {
        return getProtoKind().convertToElementKind();
    }

    @Override
    public Set<Modifier> getModifiers() {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean signatureEquals(ElementHandle eh) {
        return false;
    }

    @Override
    public final OffsetRange getOffsetRange(ParserResult result) {
        int start = result.getSnapshot().getOriginalOffset(offsetRange.getStart());
        if (start < 0) {
            return OffsetRange.NONE;
        }
        int end = result.getSnapshot().getOriginalOffset(offsetRange.getEnd());
        return new OffsetRange(start, end);
    }

    @Override
    public Kind getProtoKind() {
        switch (protoSyntaxElement.getType()) {
            case SERVICE:
                return Kind.SERVICE;
            case ENUM:
                return Kind.ENUM;
            case MESSAGE:
                return Kind.MESSAGE;
            case RPC:
                return Kind.RPC;
            case ONEOF :
                return Kind.ONEOF;
            case MAP :
            case MESSAGE_FIELD :
            case ENUM_FIELD :
            case ONEOF_FIELD :
                return Kind.VARIABLE;
            default:
                throw new IllegalStateException("Unknown ProtoElement.Kind protoSyntaxElement:" + protoSyntaxElement.toString());
        }
    }

    @Override
    public ProtoSyntaxElement getSyntaxElement() {
        return protoSyntaxElement;
    }

}
