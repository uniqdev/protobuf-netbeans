package sk.uniq.protobuf.netbeans.highlights;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import sk.uniq.protobuf.schema.ProtoBaseSchemaVisitor;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoEnumField;
import sk.uniq.protobuf.schema.ProtoMessage;
import sk.uniq.protobuf.schema.ProtoMessageField;
import sk.uniq.protobuf.schema.ProtoRpc;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;

/**
 *
 * @author jakub
 */
public class SemanticAnalysisVisitor extends ProtoBaseSchemaVisitor<Void> {

    private final Map<OffsetRange, Set<ColoringAttributes>> highlights;

    public SemanticAnalysisVisitor() {
        this.highlights = new HashMap<>();
    }
    
    public Map<OffsetRange, Set<ColoringAttributes>> getHighlights() {
        return highlights;
    }

    @Override
    public void visit(ProtoMessageField messageField, Void context) throws Exception {
        super.visit(messageField, context);
        EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.FIELD);
        highlights.put(offsetRange(messageField.getName()), attributes);
    }

    @Override
    public void visit(ProtoMessage message, Void context) throws Exception {
        super.visit(message, context); 
        EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.CLASS);
        highlights.put(offsetRange(message.getName()), attributes);
    }

    @Override
    public void visit(ProtoEnum enumPB, Void context) throws Exception {
        super.visit(enumPB, context); 
        EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.ENUM);
        highlights.put(offsetRange(enumPB.getName()), attributes);
    }    
    
    @Override
    public void visit(ProtoEnumField enumField, Void context) throws Exception {
        super.visit(enumField, context); 
        EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.FIELD);
        highlights.put(offsetRange(enumField.getName()), attributes);
    }    

    @Override
    public void visit(ProtoRpc rpc, Void context) throws Exception {
        super.visit(rpc, context); 
        EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.METHOD);
        highlights.put(offsetRange(rpc.getName()), attributes);
    }
    
    private OffsetRange offsetRange(ProtoSyntaxElement element) {
        return new OffsetRange(element.getPosition().getStartPos(), element.getPosition().getEndPos() + 1);
    }

}
