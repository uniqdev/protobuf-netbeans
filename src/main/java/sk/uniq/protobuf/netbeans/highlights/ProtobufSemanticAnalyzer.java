/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans.highlights;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import sk.uniq.protobuf.netbeans.ProtobufParserResult;

/**
 *
 * @author jherkel
 */
public class ProtobufSemanticAnalyzer extends SemanticAnalyzer {

    private boolean cancelled;
    private Map<OffsetRange, Set<ColoringAttributes>> semanticHighlights;

    public ProtobufSemanticAnalyzer() {
    }

    @Override
    public Map getHighlights() {
        return semanticHighlights;
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    protected final synchronized boolean isCancelled() {
        return cancelled;
    }

    protected final synchronized void resume() {
        cancelled = false;
    }

    @Override
    public synchronized void cancel() {
        cancelled = true;
    }

    @Override
    public void run(Result result, SchedulerEvent event) {
        resume();

        if (isCancelled()) {
            return;
        }

        ProtobufParserResult ppr = (ProtobufParserResult) result;
        if (ppr == null) {
            this.semanticHighlights = Collections.emptyMap();
            return;
        }

        SemanticAnalysisVisitor visitor = new SemanticAnalysisVisitor();
        try {
            visitor.visit(ppr.getProtoFile(), null);
        } catch (Exception ex) {
            this.semanticHighlights = Collections.EMPTY_MAP;
            return;
        }
        if (isCancelled()) {
            return;
        }
        this.semanticHighlights = visitor.getHighlights();
    }

}
