/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans.formatting;

import java.util.LinkedList;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.Exceptions;
import sk.uniq.protobuf.netbeans.ProtobufParserResult;
import sk.uniq.protobuf.netbeans.ProtobufTokenId;

/**
 * @author herkel
 */
public class ProtoDocumentFormatter {

    private final ProtoFormatter formatter;
    private final ProtobufParserResult parseResult;
    private final BaseDocument document;
    private final Context context;

    ProtoDocumentFormatter(ProtoFormatter formatter, ProtobufParserResult parseResult, BaseDocument document, Context context) {
        this.formatter = formatter;
        this.parseResult = parseResult;
        this.document = document;
        this.context = context;
    }

    public void format() {
    }
}
