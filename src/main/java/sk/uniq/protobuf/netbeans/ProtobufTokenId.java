/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import sk.uniq.protobuf.grammar.ProtoLexer;

/**
 *
 * @author herkel
 */
public class ProtobufTokenId implements TokenId {

    public enum Type {
        SYNTAX_LITERAL(ProtoLexer.SYNTAX_LITERAL, "keyword"),
        PACKAGE_LITERAL(ProtoLexer.PACKAGE_LITERAL, "keyword"),
        IMPORT_LITERAL(ProtoLexer.IMPORT_LITERAL, "keyword"),
        OPTION_LITERAL(ProtoLexer.OPTION_LITERAL, "keyword"),
        ENUM_LITERAL(ProtoLexer.ENUM_LITERAL, "keyword"),
        MESSAGE_LITERAL(ProtoLexer.MESSAGE_LITERAL, "keyword"),
        ONEOF_LITERAL(ProtoLexer.ONEOF_LITERAL, "keyword"),
        MAP_LITERAL(ProtoLexer.MAP_LITERAL, "keyword"),
        EXTENSIONS_TO_LITERAL(ProtoLexer.EXTENSIONS_TO_LITERAL, "keyword"),
        EXTENSIONS_MAX_LITERAL(ProtoLexer.EXTENSIONS_MAX_LITERAL, "keyword"),
        REPEATED_LITERAL(ProtoLexer.REPEATED_LITERAL, "keyword"),
        RESERVED_LITERAL(ProtoLexer.RESERVED_LITERAL, "keyword"),
        SERVICE_LITERAL(ProtoLexer.SERVICE_LITERAL, "keyword"),
        RETURNS_LITERAL(ProtoLexer.RETURNS_LITERAL, "keyword"),
        RPC_LITERAL(ProtoLexer.RPC_LITERAL, "keyword"),
        STREAM_LITERAL(ProtoLexer.STREAM_LITERAL, "keyword"),
        PUBLIC_LITERAL(ProtoLexer.PUBLIC_LITERAL, "keyword"),
        WEAK_LITERAL(ProtoLexer.WEAK_LITERAL, "keyword"),
        BLOCK_OPEN(ProtoLexer.BLOCK_OPEN, "operator"),
        BLOCK_CLOSE(ProtoLexer.BLOCK_CLOSE, "operator"),
        PAREN_OPEN(ProtoLexer.PAREN_OPEN, "operator"),
        PAREN_CLOSE(ProtoLexer.PAREN_CLOSE, "operator"),
        BRACKET_OPEN(ProtoLexer.BRACKET_OPEN, "operator"),
        BRACKET_CLOSE(ProtoLexer.BRACKET_CLOSE, "operator"),
        MAP_OPEN(ProtoLexer.MAP_OPEN, "operator"),
        MAP_CLOSE(ProtoLexer.MAP_CLOSE, "operator"),
        ASSIGN(ProtoLexer.ASSIGN, "operator"),
        COLON(ProtoLexer.COLON, "operator"),
        COMMA(ProtoLexer.COMMA, "operator"),
        DOT(ProtoLexer.DOT, "operator"),
        PLUS(ProtoLexer.PLUS, "operator"),
        MINUS(ProtoLexer.MINUS, "operator"),
        QUOTE(ProtoLexer.QUOTE,"string"),
        ITEM_TERMINATOR(ProtoLexer.ITEM_TERMINATOR, "keyword"),
        DOUBLE_TYPE_LITERAL(ProtoLexer.DOUBLE_TYPE_LITERAL, "literal"),
        FLOAT_TYPE_LITERAL(ProtoLexer.FLOAT_TYPE_LITERAL, "literal"),
        INT32_TYPE_LITERAL(ProtoLexer.INT32_TYPE_LITERAL, "literal"),
        INT64_TYPE_LITERAL(ProtoLexer.INT64_TYPE_LITERAL, "literal"),
        UINT32_TYPE_LITERAL(ProtoLexer.UINT32_TYPE_LITERAL, "literal"),
        UINT64_TYPE_LITERAL(ProtoLexer.UINT64_TYPE_LITERAL, "literal"),
        SINT32_TYPE_LITERAL(ProtoLexer.SINT32_TYPE_LITERAL, "literal"),
        SINT64_TYPE_LITERAL(ProtoLexer.SINT64_TYPE_LITERAL, "literal"),
        FIXED32_TYPE_LITERAL(ProtoLexer.FIXED32_TYPE_LITERAL, "literal"),
        FIXED64_TYPE_LITERAL(ProtoLexer.FIXED64_TYPE_LITERAL, "literal"),
        SFIXED32_TYPE_LITERAL(ProtoLexer.SFIXED32_TYPE_LITERAL, "literal"),
        SFIXED64_TYPE_LITERAL(ProtoLexer.SFIXED64_TYPE_LITERAL, "literal"),
        BOOLEAN_TYPE_LITERAL(ProtoLexer.BOOLEAN_TYPE_LITERAL, "literal"),
        STRING_TYPE_LITERAL(ProtoLexer.STRING_TYPE_LITERAL, "literal"),
        BYTES_TYPE_LITERAL(ProtoLexer.BYTES_TYPE_LITERAL, "literal"),
        INTEGER_LITERAL(ProtoLexer.INTEGER_LITERAL, "number"),
        STRING_LITERAL(ProtoLexer.STRING_LITERAL, "string"),
        BOOLEAN_LITERAL(ProtoLexer.BOOLEAN_LITERAL, "literal"),
        FLOAT_LITERAL(ProtoLexer.FLOAT_LITERAL, "float"),
        IDENTIFIER(ProtoLexer.IDENTIFIER, "identifier"),
        LINE_COMMENT(ProtoLexer.LINE_COMMENT, "comment"),
        WHITESPACE(ProtoLexer.WHITESPACE, "whitespace"),
        NEWLINE(ProtoLexer.NEWLINE,"whitespace"),
        ERR_CHAR(ProtoLexer.ERR_CHAR, "error");

        private final int id;
        private final String category;
        private final String text;

        private Type(int id, String category) {
            this.id = id;
            this.category = category;
            this.text = "";
        }

        public int getId() {
            return id;
        }

        public String getCategory() {
            return category;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return "Type{" + "id=" + id + ", category=" + category + ", text=" + text + '}';
        }
    }

    public static final Language<ProtobufTokenId> LANGUAGE = new ProtobufLanguageHierarchy().language();
    private final Type type;

    public ProtobufTokenId(Type type) {
        this.type = type;
    }

    @Override
    public String primaryCategory() {
        return type.getCategory();
    }

    @Override
    public int ordinal() {
        return type.getId();
    }

    public Type getType() {
        return type;
    }

    @Override
    public String name() {
        return type.name();
    }

    public static Language<ProtobufTokenId> getLanguage() {
        return LANGUAGE;
    }

}
