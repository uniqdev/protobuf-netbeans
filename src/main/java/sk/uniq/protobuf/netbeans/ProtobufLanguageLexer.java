/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.IntegerStack;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import sk.uniq.protobuf.grammar.ProtoLexer;

/**
 *
 * @author herkel
 */
public class ProtobufLanguageLexer implements Lexer<ProtobufTokenId> {

    private final LexerRestartInfo<ProtobufTokenId> info;

    private final ProtoLexer protoParserLexer;

    public ProtobufLanguageLexer(LexerRestartInfo<ProtobufTokenId> info) {
        this.info = info;
        AntlrCharStream charStream = new AntlrCharStream(info.input(), "ProtobufEditor");
        protoParserLexer = new ProtoLexer(charStream);       
        protoParserLexer.removeErrorListeners();
        LexerState lexerMode = (LexerState)info.state();
        if (lexerMode != null) {
            protoParserLexer._mode = lexerMode.mode;
            protoParserLexer._modeStack.addAll(lexerMode.stack);
        }

    }

    @Override
    public org.netbeans.api.lexer.Token<ProtobufTokenId> nextToken() {
        Token token = protoParserLexer.nextToken();
        if (token.getType() != ProtoLexer.EOF) {
            ProtobufTokenId tokenId = ProtobufLanguageHierarchy.getToken(token.getType());
            assert tokenId != null : "tokenId == null token type:" + token.getType();
            return info.tokenFactory().createToken(tokenId);
        }
        return null;
    }

    @Override
    public Object state() {
        return new LexerState(protoParserLexer._mode, protoParserLexer._modeStack);
    }

    @Override
    public void release() {
    }

    private static class LexerState {

        public int mode = -1;
        public IntegerStack stack = null;

        public LexerState(int mode, IntegerStack stack) {
            this.mode = mode;
            this.stack = new IntegerStack(stack);
        }
    }
}
