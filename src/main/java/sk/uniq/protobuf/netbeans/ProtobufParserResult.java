/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import sk.uniq.protobuf.schema.ProtoFile;

/**
 *
 * @author jherkel
 */
public class ProtobufParserResult extends ParserResult {

    private final List<org.netbeans.modules.csl.api.Error> errors = new ArrayList<>();
    private List<? extends StructureItem> items;
    private final ProtoFile protoFile;

    public ProtobufParserResult(ProtoFile protoFile, List<org.netbeans.modules.csl.api.Error> errors, Snapshot snapshot, boolean valid) {
        super(snapshot);
        this.protoFile = protoFile;
        this.errors.addAll(errors);
    }

    public void addError(org.netbeans.modules.csl.api.Error error) {
        errors.add(error);
    }
    
    public ProtoFile getProtoFile() {
        return protoFile;
    }

    @Override
    protected void invalidate() {
    }

    public synchronized List<? extends StructureItem> getItems() {
        if (items == null) {
            items = new ProtobufScanner().scanStructure(this);
        }

        return items;
    }

    public synchronized void setItems(List<? extends StructureItem> items) {
        this.items = items;
    }

    @Override
    public List<? extends org.netbeans.modules.csl.api.Error> getDiagnostics() {
        return Collections.unmodifiableList(errors);
    }
}
