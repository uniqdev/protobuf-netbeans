/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.io.CharConversionException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.api.StructureScanner.Configuration;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.xml.XMLUtil;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoEnumField;
import sk.uniq.protobuf.schema.ProtoMessage;
import sk.uniq.protobuf.schema.ProtoFile;
import sk.uniq.protobuf.schema.ProtoMap;
import sk.uniq.protobuf.schema.ProtoMessageField;
import sk.uniq.protobuf.schema.ProtoOneOf;
import sk.uniq.protobuf.schema.ProtoOneOfField;
import sk.uniq.protobuf.schema.ProtoRpc;
import sk.uniq.protobuf.schema.ProtoService;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;
import sk.uniq.protobuf.schema.ProtoSyntaxElement.ElementType;

/**
 *
 * @author jherkel
 */
public class ProtobufScanner implements StructureScanner {

    private static final Logger LOGGER = Logger.getLogger(ProtobufScanner.class.getName());
    private static ImageIcon serviceIcon = null;
    private static ImageIcon messageIcon = null;
    private static ImageIcon enumIcon = null;
    private static ImageIcon oneofIcon = null;

    @Override
    public List<? extends StructureItem> scan(ParserResult info) {
        ProtobufParserResult result = (ProtobufParserResult) info;
        if (result != null) {
            return result.getItems();
        }

        return Collections.emptyList();
    }

    List<? extends StructureItem> scanStructure(ProtobufParserResult parserResult) {
        return ProtobufStructureItem.initialize(parserResult);
    }

    @Override
    public Map<String, List<OffsetRange>> folds(ParserResult info) {
        ProtobufParserResult result = (ProtobufParserResult) info;
        if (result == null) {
            return Collections.emptyMap();
        }

        List<? extends StructureItem> items = result.getItems();
        if (items.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, List<OffsetRange>> folds = new HashMap<>();
        List<OffsetRange> codeblocks = new ArrayList<>();
        folds.put("tags", codeblocks); // NOI18N

        BaseDocument doc = (BaseDocument) result.getSnapshot().getSource().getDocument(false);

        for (StructureItem item : items) {
            try {
                addBlocks(result, doc, result.getSnapshot().getText(), codeblocks, item);
            } catch (BadLocationException ble) {
                Exceptions.printStackTrace(ble);
                break;
            }
        }

        return folds;
    }

    private void addBlocks(ProtobufParserResult result, BaseDocument doc, CharSequence text, List<OffsetRange> codeblocks, StructureItem item) throws BadLocationException {
        int docLength = doc == null ? text.length() : doc.getLength();
        int begin = Math.min((int) item.getPosition(), docLength);
        int end = Math.min((int) item.getEndPosition(), docLength);
        int firstRowEnd = doc == null ? GsfUtilities.getRowEnd(text, begin) : Utilities.getRowEnd(doc, begin);
        int lastRowEnd = doc == null ? GsfUtilities.getRowEnd(text, end) : Utilities.getRowEnd(doc, end);
        if (begin < end && firstRowEnd != lastRowEnd) {
            codeblocks.add(new OffsetRange(firstRowEnd, end));
        } else {
            return;
        }

        for (StructureItem child : item.getNestedItems()) {
            int childBegin = (int) child.getPosition();
            int childEnd = (int) child.getEndPosition();
            if (childBegin >= begin && childEnd <= end) {
                addBlocks(result, doc, text, codeblocks, child);
            }
        }
    }

    @Override
    public Configuration getConfiguration() {
        return new Configuration(false, false, 0);
    }

    private static class ProtobufStructureItem implements StructureItem, Comparable<ProtobufStructureItem> {

        private final String name;
        private final List<ProtobufStructureItem> children;
        private final ProtoElement element;

        ProtobufStructureItem(ProtoElement element, String name) {
            this.element = element;
            this.name = name;
            this.children = Collections.EMPTY_LIST;
        }

        ProtobufStructureItem(ProtoElement element, String name, List<ProtobufStructureItem> children) {
            this.element = element;
            this.name = name;
            this.children = Collections.unmodifiableList(new ArrayList<>(children));
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getSortText() {
            return getName();
        }

        @Override
        public String getHtml(HtmlFormatter formatter) {
            String s = getName();
            try {
                return XMLUtil.toElementContent(s);
            } catch (CharConversionException cce) {
                LOGGER.log(Level.FINE, "NAME:" + s, cce);
                return s;
            }
        }

        @Override
        public ElementHandle getElementHandle() {
            return element;
        }

        @Override
        public ElementKind getKind() {
            return element.getKind();
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.EMPTY_SET;
        }

        @Override
        public boolean isLeaf() {
            return getNestedItems().isEmpty();
        }

        private static List<? extends StructureItem> initialize(ProtobufParserResult parserResult) {
            FileObject fileObject = parserResult.getSnapshot().getSource().getFileObject();
            ProtoFile protoFile = parserResult.getProtoFile();
            List<StructureItem> children = new ArrayList<>();
            for (ProtoSyntaxElement element : protoFile.getFilteredNestedElements(EnumSet.of(ElementType.MESSAGE, ElementType.ENUM, ElementType.SERVICE))) {
                switch (element.getType()) {
                    case MESSAGE:
                        List<ProtobufStructureItem> msgNestedItems = processNestedMessageElements((ProtoMessage) element, fileObject);
                        ProtobufStructureItem msgObj = createStructureItem((ProtoMessage) element, ((ProtoMessage) element).getName().getName(), fileObject, msgNestedItems);
                        children.add(msgObj);
                        break;
                    case ENUM:
                        List<ProtobufStructureItem> enumNestedItems = processFields(element, fileObject);
                        ProtobufStructureItem enumObj = createStructureItem((ProtoEnum) element, ((ProtoEnum) element).getName().getName(), fileObject,enumNestedItems);
                        children.add(enumObj);
                        break;
                    case SERVICE:
                        ProtoService service = (ProtoService) element;
                        List<ProtobufStructureItem> srvNestedItems = null;
                        List<ProtoRpc> rpcs = service.getFilteredNestedElements(ElementType.RPC);
                        if (rpcs.isEmpty() == false) {
                            srvNestedItems = new ArrayList<>(rpcs.size());
                            for (ProtoRpc rpc : rpcs) {
                                ProtobufStructureItem rpcObj = createStructureItem(rpc, rpc.getName().getName(), fileObject);
                                srvNestedItems.add(rpcObj);
                            }
                        }
                        ProtobufStructureItem serviceObj = createStructureItem(service, service.getName().getName(), fileObject, srvNestedItems);
                        children.add(serviceObj);
                        break;
                    default:
                        break;
                }
            }
            return children;
        }

        private static List<ProtobufStructureItem> processNestedMessageElements(ProtoMessage nestedMessage, FileObject fileObject) {
            List<ProtobufStructureItem> nestedItems = new ArrayList<>();
            for (ProtoSyntaxElement element : nestedMessage.getFilteredNestedElements(EnumSet.of(ElementType.MESSAGE, ElementType.ENUM,ElementType.ONEOF))) {
                switch (element.getType()) {
                    case MESSAGE:
                        List<ProtobufStructureItem> msgNestedItems = processNestedMessageElements((ProtoMessage) element, fileObject);
                        ProtobufStructureItem msgObj = createStructureItem((ProtoMessage) element, ((ProtoMessage) element).getName().getName(), fileObject, msgNestedItems);
                        nestedItems.add(msgObj);
                        break;
                    case ENUM:
                        List<ProtobufStructureItem> enumNestedItems = processFields(element, fileObject);
                        ProtobufStructureItem enumObj = createStructureItem((ProtoEnum) element, ((ProtoEnum) element).getName().getName(), fileObject,enumNestedItems);
                        nestedItems.add(enumObj);
                        break;
                    case ONEOF:
                        List<ProtobufStructureItem> oneofNestedItems = processFields(element, fileObject);
                        ProtobufStructureItem oneofObj = createStructureItem((ProtoOneOf) element, ((ProtoOneOf) element).getName().getName(), fileObject,oneofNestedItems);
                        processFields(element, fileObject);
                        nestedItems.add(oneofObj);
                        break;
                    default:
                        break;
                }
            }
            nestedItems.addAll(processFields(nestedMessage,fileObject));
            return nestedItems;
        }

        private static List<ProtobufStructureItem> processFields(ProtoSyntaxElement parentElement, FileObject fileObject) {
            List<ProtobufStructureItem> nestedItems = new ArrayList<>();
            for (ProtoSyntaxElement element : parentElement.getFilteredNestedElements(EnumSet.of(ElementType.ENUM_FIELD, ElementType.MESSAGE_FIELD, ElementType.ONEOF_FIELD))) {
                switch (element.getType()) {
                    case ENUM_FIELD:
                        ProtobufStructureItem enumField = createStructureItem((ProtoEnumField) element, ((ProtoEnumField) element).getName().getName(), fileObject);
                        nestedItems.add(enumField);
                        break;
                    case MESSAGE_FIELD:
                        ProtobufStructureItem msgField = createStructureItem((ProtoMessageField) element, ((ProtoMessageField) element).getName().getName(), fileObject);
                        nestedItems.add(msgField);
                        break;
                    case ONEOF_FIELD:
                        ProtobufStructureItem oneOfField = createStructureItem((ProtoOneOfField) element, ((ProtoOneOfField) element).getName().getName(), fileObject);
                        nestedItems.add(oneOfField);
                        break;
                    case MAP:
                        ProtobufStructureItem mapField = createStructureItem((ProtoMap) element, ((ProtoMap) element).getName().getName(), fileObject);
                        nestedItems.add(mapField);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected type:" + element.getType());
                }
            }
            return nestedItems;
        }

        public static <T extends ProtoSyntaxElement> ProtobufStructureItem createStructureItem(T object, String name, FileObject fileObject) {
            return createStructureItem(object, name, fileObject, null);
        }

        public static <T extends ProtoSyntaxElement> ProtobufStructureItem createStructureItem(T object, String name, FileObject fileObject, List<ProtobufStructureItem> children) {
            ProtoElement protoElement = new ProtoElementImpl(object, fileObject, name);
            return children != null
                    ? new ProtobufStructureItem(protoElement, name, children)
                    : new ProtobufStructureItem(protoElement, name);
        }

        @Override
        public List<? extends StructureItem> getNestedItems() {
            assert children != null;
            return children;
        }

        @Override
        public long getPosition() {
            return element.getSyntaxElement().getPosition().getStartPos();
        }

        @Override
        public long getEndPosition() {
            return element.getSyntaxElement().getPosition().getEndPos() + 1;
        }

        @Override
        public ImageIcon getCustomIcon() {
            switch (element.getProtoKind()) {
                case SERVICE:
                    if (serviceIcon == null) {
                        serviceIcon = new ImageIcon(ImageUtilities.loadImage("sk/uniq/protobuf/netbeans/serviceIcon.png")); //NOI18N
                    }
                    return serviceIcon;
                case MESSAGE:
                    if (messageIcon == null) {
                        messageIcon = new ImageIcon(ImageUtilities.loadImage("sk/uniq/protobuf/netbeans/messageIcon.png")); //NOI18N
                    }
                    return messageIcon;
                case ENUM:
                    if (enumIcon == null) {
                        enumIcon = new ImageIcon(ImageUtilities.loadImage("sk/uniq/protobuf/netbeans/enumIcon.png")); //NOI18N
                    }
                    return enumIcon;
                case ONEOF :
                    if (oneofIcon == null) {
                        oneofIcon = new ImageIcon(ImageUtilities.loadImage("sk/uniq/protobuf/netbeans/oneofIcon.png")); //NOI18N
                    }
                    return oneofIcon;
                default:
                    return null;
            }
        }

        @Override
        public int compareTo(ProtobufStructureItem other) {
            return (int) (getPosition() - other.getPosition());
        }

    }
}
