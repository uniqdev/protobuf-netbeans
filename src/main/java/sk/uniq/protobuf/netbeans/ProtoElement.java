/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;

/**
 *
 * @author jherkel
 */
public interface ProtoElement extends ElementHandle {

    public enum Kind {

        SERVICE(1),
        RPC(2),
        MESSAGE(3),
        ENUM(4),
        ONEOF(5),
        VARIABLE(6);

        private final int id;
        private static final Map<Integer, Kind> LOOKUP = new HashMap<Integer, Kind>();

        static {
            for (Kind kind : Kind.values()) {
                LOOKUP.put(kind.getId(), kind);
            }
        }

        private Kind(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static Kind fromId(int id) {
            return LOOKUP.get(id);
        }

        public ElementKind convertToElementKind() {
            switch (this) {
                case SERVICE:
                case RPC:
                    return ElementKind.METHOD;
                case MESSAGE:
                case ENUM:
                case ONEOF :
                    return ElementKind.CLASS;
                case VARIABLE:
                    return ElementKind.VARIABLE;
                default:
                    throw new IllegalStateException("unknow conversion ProtoElement:" + this.name());
            }
        }

    }
    
    public Kind getProtoKind();
    
    public ProtoSyntaxElement getSyntaxElement();

}
