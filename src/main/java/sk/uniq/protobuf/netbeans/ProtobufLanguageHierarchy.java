/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import sk.uniq.protobuf.netbeans.ProtobufTokenId.Type;

/**
 *
 * @author herkel
 */
public class ProtobufLanguageHierarchy extends LanguageHierarchy<ProtobufTokenId> {

    private static final List<ProtobufTokenId> TOKENS = new ArrayList<>();
    private static final Map<Integer, ProtobufTokenId> ID_TO_TOKEN = new HashMap<>();

    static {
        Type[] tokenTypes = Type.values();
        for (Type tokenType : tokenTypes) {
            TOKENS.add(new ProtobufTokenId(tokenType));
        }
        for (ProtobufTokenId tokenId : TOKENS) {
            ID_TO_TOKEN.put(tokenId.ordinal(), tokenId);
        }
    }

    static synchronized ProtobufTokenId getToken(int id) {
        return ID_TO_TOKEN.get(id);
    }

    @Override
    protected synchronized Collection<ProtobufTokenId> createTokenIds() {
        return TOKENS;
    }

    @Override
    protected synchronized Lexer<ProtobufTokenId> createLexer(LexerRestartInfo<ProtobufTokenId> info) {
        return new ProtobufLanguageLexer(info);
    }

    @Override
    protected String mimeType() {
        return ProtobufContant.PROTO_MIME_TYPE;
    }
}
