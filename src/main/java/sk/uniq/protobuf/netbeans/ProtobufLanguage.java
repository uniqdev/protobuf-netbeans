/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import sk.uniq.protobuf.netbeans.highlights.ProtobufSemanticAnalyzer;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.MIMEResolver;
import sk.uniq.protobuf.netbeans.formatting.ProtoFormatter;

/**
 *
 * @author jherkel
 */

@MIMEResolver.ExtensionRegistration(displayName = "Protocol Buffers",
extension = {"proto"},
mimeType = ProtobufContant.PROTO_MIME_TYPE,
position = 280)
@LanguageRegistration(mimeType = ProtobufContant.PROTO_MIME_TYPE) //NOI18N
public class ProtobufLanguage extends DefaultLanguageConfig {

    @Override
    public Language getLexerLanguage() {
        return ProtobufTokenId.getLanguage();
    }

    @Override
    public String getPreferredExtension() {
        return "proto";
    }

    @Override
    public String getDisplayName() {
        return "Protocol Buffers";
    }

    @Override
    public String getLineCommentPrefix() {
        return "//"; // NOI18N
    }

    @Override
    public Parser getParser() {
        return new ProtobufParser();
    }

    @Override
    public boolean hasStructureScanner() {
        return true;
    }

    @Override
    public StructureScanner getStructureScanner() {
        return new ProtobufScanner();
    }

    @Override
    public SemanticAnalyzer getSemanticAnalyzer() {
        return new ProtobufSemanticAnalyzer();
    }

    @Override
    public KeystrokeHandler getKeystrokeHandler() {
        return null; //new ProtobufKeystrokeHandler();
    }

    @Override
    public CodeCompletionHandler getCompletionHandler() {
        return null;
    }

    @Override
    public InstantRenamer getInstantRenamer() {
        return null;
    }

    @Override
    public boolean hasFormatter() {
        return true;
    }
    
    @Override
    public Formatter getFormatter() {
        return new ProtoFormatter();
    }
    
}