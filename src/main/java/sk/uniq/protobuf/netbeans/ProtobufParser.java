/* 
 * Copyright (C) 2016 Jakub Herkel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sk.uniq.protobuf.netbeans;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.csl.spi.DefaultError;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.modules.csl.api.Severity;
import sk.uniq.protobuf.parser.ProtoParserError;
import sk.uniq.protobuf.parser.ProtoParserOptions;
import sk.uniq.protobuf.schema.ProtoFile;

/**
 *
 * @author jherkel
 */
class ProtobufParser extends Parser {

    private Snapshot snapshot;
    private ProtoFile protoFile;
    private final List<org.netbeans.modules.csl.api.Error> errors = new ArrayList<>();

    @Override
    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) {
        ProtobufSyntaxErrorListener listener = new ProtobufSyntaxErrorListener();
        this.snapshot = snapshot;
        this.errors.clear();

        try {
            sk.uniq.protobuf.parser.ProtoParser parser = new sk.uniq.protobuf.parser.impl.ProtoParserImpl(snapshot.getSource().getFileObject().getName(), ProtoParserOptions.DEFAULT);
            parser.addListener(listener);
            protoFile = parser.parse(snapshot.getText().toString()).getProtoFile();
            convertErrors(listener.getErrors());
        } catch (Exception ex) {
            convertErrors(listener.getErrors());
            ex.printStackTrace();
        }
    }

    @Override
    public Result getResult(Task task) {
        return new ProtobufParserResult(protoFile, errors, snapshot, true);
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
    }

    private void convertErrors(List<ProtoParserError> syntaxErrors) {
        syntaxErrors.stream().forEach(
                error -> errors.add(new DefaultError(null, error.getMessage(), error.getMessage(), snapshot.getSource().getFileObject(),
                        error.getPosition().getStartPos(), error.getPosition().getEndPos() + 1, false, Severity.ERROR)));
    }

}
