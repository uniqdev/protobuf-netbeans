# Netbeans Protocol buffers plugin #
A NetBeans IDE plugin which adds Protocol Buffers support. 

### Installation

Download the latest nbm file from the [Downloads](https://bitbucket.org/uniqdev/protobuf-netbeans/downloads) page. 

### Features
* Syntax highlighting and error checking
* Code folding
* Provides bread crumbs in the editor

### Requirement
* Java 8+
* NetBeans 8.1+

### Build
mvn clean package